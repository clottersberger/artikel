/**
 * Ein Artikel-Programm in Java.
 * Dies ist ein Javadoc-Kommentar.
 *
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 14.10.2020
 */

package org.example;

//Interface
public interface ArtikelDAO {

    //Methodenköpfe
    public Artikel insert(Artikel a);

    public void update(Artikel a);

    public void delete(Artikel a);
}
