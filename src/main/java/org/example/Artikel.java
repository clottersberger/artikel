/**
 * Ein Artikel-Programm in Java.
 * Dies ist ein Javadoc-Kommentar.
 *
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 14.10.2020
 */

package org.example;

//Importieren
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

//Klasse
public class Artikel implements ArtikelDAO {

    //Eigenschaften/Attribute
    private int nummer;
    private String name;

    //Methoden
    /**
     *
     * @param a Artikel wird eingetragen
     * @return Liefert den Artikel
     */
    @Override
    public Artikel insert(Artikel a) {

        //Bauer Query zusammen
        String query = "insert into Lager (nummer, name)"
                + " values (?, ?)";
        return a;
    }

    /**
     *
     * @param a Artikel wird upgedatet
     */
    @Override
    public void update(Artikel a) {
        //Tabelle updaten
        String update = "update Lager set Lager_nummer = ?," +
                "Lager_name = ?"
                + " values (?, ?)";
    }

    /**
     *
     * @param a löscht den Artikel
     */
    @Override
    public void delete(Artikel a) {
        //Tabelle löschen
        String delete = "delete from Lager where Lager_nummer = ?," +
                "Lager_name = ?";
    }
}
